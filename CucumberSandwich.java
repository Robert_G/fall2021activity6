public class CucumberSandwich extends VegetarianSandwich {
    public String getProtein(){
        return "Cucumber";
    }

    public boolean isVegetarian(){
        return false;
    }
}
