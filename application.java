public class application {
    public static void main(String[] args){
        ISandwich b = new BagelSandwich();
        System.out.println(b.getFilling());
        b.addFilling("Cream Cheese");
        System.out.println(b.getFilling());
        b.addFilling("Jam");
        System.out.println(b.getFilling());
        //VegetarianSandwich vs = new VegetarianSandwich();
        ISandwich tofu = new TofuSandwich();
        if(tofu instanceof ISandwich){
            System.out.println("Yes its an instance of ISandwich");
        }
        if(tofu instanceof VegetarianSandwich){
            System.out.println("Yes its an instance of VegetarianSandwich");
        }
        //tofu.addFilling("chicken"); //Compiler error. Contains non-vegetarian topping
        System.out.println(((TofuSandwich)tofu).isVegan());

        VegetarianSandwich vege = new CucumberSandwich();
        System.out.println(vege.isVegetarian());
    }
}
