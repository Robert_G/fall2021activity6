public abstract class VegetarianSandwich implements ISandwich {
    private String filling;

    public VegetarianSandwich(){
        this.filling = "";
    }

    public String getFilling() {
       return this.filling;
    }

    public void addFilling(String topping){
        String[] notAllowed = new String[]{"chicken","beef","fish","meat","pork"};
        for(String elem : notAllowed){
            if(topping == elem){
                throw new IllegalArgumentException("Contains non vegetarian toppings");
            }
        }
        this.filling += topping+" ";
    }

    public final boolean isVegetarian(){
        return true;
    }

    public boolean isVegan(){
        if(this.filling.indexOf("cheese") < 0 || this.filling.indexOf("egg") < 0){
            return true;
        }
        return false;
    }

    public String getProtein(){
        return "";
    }
}
